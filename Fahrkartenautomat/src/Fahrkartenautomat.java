﻿import java.util.Scanner;

class Fahrkartenautomat{
	
    public static void main(String[] args){
    	
    	System.out.print("\n");
    	
    	//Erstbestellung aufnehmen
    	//------------------------
    	
    	double summe = fahrkartenbestellungErfassen(); //Variable für Summe/Preis der Ticketbestellung
    	
    	System.out.printf("\n" + "Aktuelle Summe: %.2f € \n", summe);
    	
    	//weitere Bestellungen entgegennehmen
    	
    	while(abfragen() != false) {
			
			summe += fahrkartenbestellungErfassen(); //Erfassung und Zusammenrechnung der Ticketpreise
			
			System.out.printf("\n" + "Aktuelle Summe: %.2f € \n", summe); //Ausgabe der aktuellen Summe
		
		}
    	
    	System.out.printf("Gesamtsumme: %.2f €", summe); //Ausgabe der Gesamtsumme
    	
    	rueckgeldAusgeben(fahrkartenBezahlen(summe)); //Bezahlung und Ausgabe des Rückgelds
    	
    	sc.close();
    	
    	//muenzenGrafisch(10);
    	
	}
    
    public static Scanner sc = new Scanner(System.in);
    
    //Methode, die die Bestellung der Fahrkarten erfasst
    //--------------------------------------------------
    
    public static double fahrkartenbestellungErfassen() {
    	
    	double ticketpreis = fahrkartenArt();
    	
    	//Eingabe Anzahl der Fahrkarten
    	//------------------------------
    	
    	int anzahlTickets = 0;
    	double zuZahlenderBetrag = 0.0;
    	
    	System.out.print("Anzahl der Tickets (mind. 1/max. 10): ");
    	
    	anzahlTickets = sc.nextInt(); //Anzahl der Tickets wird über die Eingabe erfasst und in einer Variable
    	
    	//Ticketgrenze --> mind. 1 Fahkarte und max. 10 Fahrkarten aufeinmal zum Kauf zulässig --> wird über Methode 'eingabeRichtig()' überprüft
    	//------------------------------------------------------------------------------------
    	
    	anzahlTickets = (int) eingabeRichtig(anzahlTickets, 1, 10);
 
		zuZahlenderBetrag = anzahlTickets * ticketpreis; //Gesamtpreis aus Anzahl der Tickets und dem dazugehörigen Preis wird berechnet und in einer Variable gespeichert
    	
    	return zuZahlenderBetrag; //der zu zahlende Betrag wird als Rückgabewert für die Methode 'fahrkartenBezahlen()' zurückgegeben
    	
    }
    
    //Methode für die Bezahlung der Fahrkarten
    //----------------------------------------
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	    	
    	//Benutzer bitten, das Geld für die Bezahlung einzuwerfen und darauf hinweisen, dass er mind. 5 Cent und höchstens 20 Euro einwerfen kann
    	//-----------------------------------------------------------------------------------------------------------------------------------
    	
    	double eingeworfenesGeld = 0.0, rueckgabebetrag = 0.0; //Erzeugung von Variablen für die eingeworfenen Münzen und für den Wert des Rückgelds

    	System.out.println("\n\nBitte das Geld einwerfen... ");
    	
    	System.out.print("Eingabe (mind. 5 Ct, höchstens 50,00 €): ");
    	
    	eingeworfenesGeld = sc.nextDouble();
    	
    	eingeworfenesGeld  = eingabeRichtig(eingeworfenesGeld, 0.05, 50.00);
    	
    	double eingezahlterGesamtbetrag = eingeworfenesGeld; //Variable, die den Gesamtwert des eingeworfenen Geldes zusammenrechnet
    	
       	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
        	
       		System.out.printf("\nNoch zu zahlen: %.2f € ", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
       		
       		System.out.print("\nGeldeinwurf: ");
       		eingeworfenesGeld = sc.nextDouble();
       		
   			eingezahlterGesamtbetrag += eingeworfenesGeld;
   					
       	}
       
         rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //Betrag des Rückgelds wird berechnet und als Rückgabewert für die Methode 'rueckgeldAusgeben()' zurückgegeben
      
         return rueckgabebetrag;
        
    }
    
    //Methode, die die Ausgabe des Fahrscheins/Tickets anzeigt
    //--------------------------------------------------------
   
    public static void fahrkartenAusgeben() {
    	
    	System.out.print("\n\nFahrschein wird ausgegeben ");
    	
    	for(int i = 0; i < 10; i++){
    		
    		System.out.print("=");
    		warte(230);
    		
    	}
    	
    	System.out.println("");
    	
    	System.out.println("\nVielen Dank!\n" +
				   		   "Vergessen Sie nicht, den Fahrschein\n"+
				   		   "vor Fahrtantritt entwerten zu lassen!\n"+
     		   			   "Wir wünschen Ihnen eine gute Fahrt.");
        
    }
    
    //Methode für die Ausgabe des Rückgeldes
    //--------------------------------------
    
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        
        if(rueckgabebetrag > 0.0){
     	   
     	   System.out.printf("%nDer Rückgabebetrag in Höhe von %.2f € ", rueckgabebetrag);
     	   System.out.print("wird in folgenden Münzen ausgezahlt: \n");

     	   while(rueckgabebetrag >= 50.00) { //50 EURO-Scheine
     	   
     		   //muenzeAusgeben(50.00, "€");
     		   muenzenGrafisch(50, "E U R O");
     		   rueckgabebetrag -= 50.00;
     		   warte(400);
     		   
     	   }
     	   
     	   while(rueckgabebetrag >= 20.00) { //20 EURO-Scheine
     		   
     		   //muenzeAusgeben(20.00, "€");
     		   muenzenGrafisch(20, "E U R O");
     		   rueckgabebetrag -= 20.00;
     		   warte(400);
     		   
     	   }
     	   
     	   while(rueckgabebetrag >= 10.00) { //10 EURO-Scheine
     		   
     		   //muenzeAusgeben(10.00, "€");
     		   muenzenGrafisch(10, "E U R O");
     		   rueckgabebetrag -= 10.00;
     		   warte(400);
     		   
     	   }
     	   
     	   while(rueckgabebetrag >= 5.00) { //5 EURO-Scheine
     		   
     		   //muenzeAusgeben(5.00, "€");
     		   muenzenGrafisch(5, "E U R O");
     		   rueckgabebetrag -= 5.00;
     		   warte(400);
     		   
     	   }
     	   
            while(rueckgabebetrag >= 2.00){ // 2 EURO-Münzen
            
         	  //muenzeAusgeben(2.00, "€");
              muenzenGrafisch(2, "E U R O");
 	          rueckgabebetrag -= 2.00;
 	          warte(400);
 	          
            }
            
            while(rueckgabebetrag >= 1.00){ // 1 EURO-Münzen
            
              //muenzeAusgeben(1.00, "€");
              muenzenGrafisch(1, "E U R O");
 	          rueckgabebetrag -= 1.00;
 	          warte(400);
 	          
            }
            
            while(rueckgabebetrag >= 0.50){ // 50 CENT-Münzen
            
              //muenzeAusgeben(0.50, "€");
              muenzenGrafisch(50, "C E N T");
 	          rueckgabebetrag -= 0.50;
 	          warte(400);
 	          
            }
            
            while(rueckgabebetrag >= 0.20) { // 20 CENT-Münzen
            
              //muenzeAusgeben(0.20, "€");
              muenzenGrafisch(20, "C E N T");
  	          rueckgabebetrag -= 0.20;
  	          warte(400);
  	          
            }
            
            while(rueckgabebetrag >= 0.10){ // 10 CENT-Münzen
            
              //muenzeAusgeben(0.10, "€");
              muenzenGrafisch(10, "C E N T");
 	          rueckgabebetrag -= 0.10;
 	          warte(400);
 	          
            }
            
            while(rueckgabebetrag >= 0.05){ // 5 CENT-Münzen
            
              //muenzeAusgeben(0.05, "€");
              muenzenGrafisch(5, "C E N T");
  	          rueckgabebetrag -= 0.05;
  	          warte(400);
  	          
            }
            
        }
        
        fahrkartenAusgeben(); //nach Ausgabe des Rückgeldes wird der Fahrschein ausgegeben

    }
    
    //Methode für die Ausgabe der Münzen
    //----------------------------------
    
    public static void muenzeAusgeben(double betrag, String zeichen) {
    	
    	System.out.printf("\n%.2f" + " " + zeichen, betrag);
    	
    }
    
    //Methode, um Käufer nach weiteren Kaufvorgang zu fragen
	//------------------------------------------------------
	
	public static boolean abfragen() {
		
		boolean weitererKauf = false;
		
		System.out.print("\n" + "Möchten Sie weitere Tickets kaufen? (j/n) ");
		
		String antwort = "";
		
		boolean eingabeRichtig = false;
		
		do {
			
			antwort = sc.next();
		
			if(antwort.equalsIgnoreCase("j") || antwort.equalsIgnoreCase("n")) {
			
				eingabeRichtig = true;
			
			} else {
				
				error("Bitte erneut eingeben: ");
								
			}
		
		} while(eingabeRichtig != true);
		
		
		System.out.print("\n");
		
		if(antwort.equalsIgnoreCase("j")) {

			weitererKauf = true;
			
		} else {
			
			weitererKauf = false;
			
		} 
		
		return weitererKauf;
		
	}

	//Methode, die an einer Stelle für eine gewisse Zeit wartet und danach weiterläuft
    //--------------------------------------------------------------------------------
 
    public static void warte(int millisekunden) {
    	     
    	try{
        
    		Thread.sleep(millisekunden); //Verzögerung um 250 ms
   			
        } catch(InterruptedException e){
           	  
        	// TODO Auto-generated catch block
        	e.printStackTrace();
  
        }       
    	
    }
    
    //Methode, die nach der Art der Fahrkarte fragt und den Preis der Fahrkarte(n) als Rückgabewert zurückgibt
    //-----------------------------------------------------------------------------------------------------
    
    public static double fahrkartenArt() {
    	
    	System.out.print("\t\tWählen Sie Ihre Wunschfahkarte für Berlin AB aus \n\n");
    	
    	//Mögliche Fahrtickets zum Auswählen
    	//----------------------------------
    	
    	String bezeichnung [] = {
    			
    							 "Einzelfahrschein Berlin AB",         //Nr.: 1
    							 "Einzelfahrschein Berlin BC",         //Nr.: 2
    							 "Einzelfahrschein Berlin ABC",        //Nr.: 3
    							 "Kurzstrecke",                        //Nr.: 4
    							 "Tageskarte Berlin AB",               //Nr.: 5
    							 "Tageskarte Berlin BC",               //Nr.: 6
    							 "Tageskarte Berlin ABC",              //Nr.: 7
    							 "Kleingruppen-Tageskarte Berlin AB",  //Nr.: 8
    							 "Kleingruppen-Tageskarte Berlin BC",  //Nr.: 9
    							 "Kleingruppen-Tageskarte Berlin ABC"  //Nr.: 10
    							 
    							};
    	
    	double kartenPreise [] = {
    			
    							  2.90,
    							  3.30,
    							  3.60,
    							  1.90,
    							  8.60,
    							  9.00,
    							  9.60,
    							  23.50,
    							  24.30,
    							  24,90
    							  
    							 };
    	
    	int auswahlnummer [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    	
    	//Ausgabe des Auswahlmenüs
    	//------------------------
    	
    	System.out.printf("%-30s %-45s %s", "Auswahl-Nr.:", "Bezeichnung:", "Preis in Euro:");
    	
    	for(int i = 0; i < auswahlnummer.length; i++) {
    		
    		System.out.printf("\n%-30d %-45s %.2f", auswahlnummer[i], bezeichnung[i], kartenPreise[i]);
    		
    	}
    	
    	System.out.print("\n\n" + "Auswahl: ");
    	
    	int wahl = sc.nextInt();
    	
    	double preisKarte = 0.0;
    	
    	//Überprüfung, ob Eingabe: (Aus)wahl korrekt ist
    	//----------------------------------------------
    	
    	wahl = (int) eingabeRichtig(wahl, 1, 10);
    	System.out.print("\n");
    		
		preisKarte = kartenPreise [wahl-1]; //(-1), da das erste Element des Arrays an Stelle 0 steht --> ansonsten stimmt Benutzereingabe nicht überein 
			  							    //z.B. Wahl von Einzelfahrschein [...] AB erfolgt über Zahl: 1 und der dazugehörige Preis im Array steht an Stelle: 0 und nicht an Stelle: 1
    
    	return preisKarte;
    	
    }
    
    //Methode, um zu überpüfen, ob die Eingabe korrekt ist bzw. im Interwall liegt --> untereGrenze <= eingabe <= obereGrenze
    //-----------------------------------------------------------------------------------------------------------------------
    
    public static double eingabeRichtig(double eingabe, double untereGrenze, double obereGrenze) {
    	
    	boolean eingabeRichtig = false;
    
    	while(eingabeRichtig != true) {
    		
    		if(eingabe >= untereGrenze && eingabe <= obereGrenze) {
    			
   				eingabeRichtig = true;
    			
   			} else {
   				
   				error("Bitte erneut eingeben: ");
   				
   				eingabe = sc.nextDouble();
   			
   			}
    		
    	}
    		
    	return eingabe;
    	
    }
    
    //Methode, um Fehlermeldung auszugeben
    //------------------------------------
    
    public static void error(String text) {
    	
    	System.out.print("\n" + "Eingabe ungültig.");
    	
    	System.out.print("\n" + text);
    	
    }
    
    //Methode, die die Münzen "grafisch" ausgibt
    //------------------------------------------
    
    public static void muenzenGrafisch(int betrag, String zeichen) {
    	
    	if(betrag >= 5.00 && zeichen.equals("E U R O")) { //Ausgabe für Scheine
    		
    		System.out.print("\n\n");
			
			System.out.println("* * * * * * * * * * * * * * *");
			System.out.printf("%s %28s", "*", "*" + "\n");
			System.out.printf("%-6s %-4s %s %10s", "*", betrag, zeichen, "*" + "\n");
			System.out.printf("%s %28s", "*", "*" + "\n");
			System.out.printf("%s %28s", "*", "*" + "\n");
			System.out.println("* * * * * * * * * * * * * * *" + "\n");
    		
    	} else { //Ausgabe für Münzen
    		
    		System.out.print("\n");
        	
        	System.out.printf("%11s", "* * *" + "\n");
        	System.out.printf("%3s %9s" + "\n", "*", "*");
           	System.out.printf("%2s %5d %5s" + "\n", "*", betrag, "*");
        	System.out.printf("%2s %8s %2s" + "\n", "*", zeichen, "*");
        	System.out.printf("%3s %9s" + "\n", "*", "*");
        	System.out.printf("%11s", "* * *" + "\n");
        	
        	System.out.print("\n");
    		
    	}
    	
    }

}





//Für Ticketpreis Datentyp 'double' gewählt um Preis genau mit Cents angeben zu können

/*Für Anzahl der Tickets Datentyp 'int' gewählt, da die Anzahl der Tickets nur ganzzahlig sein
kann und es dementsprechend keine halben Tickets gibt (z.B. 2,5 Tickets)*/

/* zuZahlenderBetrag = anzahlTickets * ticketpreis
 * ------------------------------------------------
 * Hier wird der Gesamtbetrag, der zu zahlen ist berechnet.
 * Dieser hängt davon ab, wie viele Fahrkarten man kauft und wie hoch der Preis des Tickets ist.
 * Dementsprechend ergibt sich die Gesamtsumme aus Anzahl der Tickets multipliziert mit dem Preis der Tickets.
 * 
 * Der rechtsstehende Ausdruck [anzahlTickets * ticketpreis] wird zunächst ausgewertet und mit der Variable
 * 'zuZahlenderBetrag' initialisiert/gespeichert.
 */
