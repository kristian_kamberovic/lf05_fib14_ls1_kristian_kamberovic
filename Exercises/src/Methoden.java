
public class Methoden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		umrechnen("USA", 1);
		umrechnen("Japan", 1);
		umrechnen("England", 1);
		umrechnen("Schweiz", 1);
		umrechnen("Schweden", 1);
				
	}
	
	public static double multiplizieren(double a, double b) {
		
		return (a*b);
		
	}
	
	public static double volumenWuerfel(double a) {
		
		double volumen = Math.pow(a, 3);
		
		return volumen;
		
	}
	
	public static double volumenQuader(double a, double b, double c) {
		
		double volumen = a*b*c;
		
		return volumen;
		
	}
	
	public static double volumenPyramide(double a, double h) {
		
		double volumen = (1/3)*a*a*h;
		
		return volumen;
		
	}
	
	public static double volumenKugel(double r) {
		
		double volumen = (4/3)*(Math.pow(r, 3)*Math.PI);
		
		return volumen;
		
	}
	
	public static double umrechnen(String gastland, double a) {
		
		double wechselBetrag = a;
				
		if(gastland.equalsIgnoreCase("USA")) {
			
			wechselBetrag *= 1.22; //1 Euro > 1,22 USD
			System.out.printf("%.2f Euro = %.2f USD (Dollar)\n", a, wechselBetrag);
			
		} else if(gastland.equalsIgnoreCase("Japan")) {
			
			wechselBetrag *= 126.50; //1 Euro > 126.50 JPY
			System.out.printf("%.2f Euro = %.2f JPY (Yen)\n", a, wechselBetrag);
			
		} else if(gastland.equalsIgnoreCase("England")) {
			
			wechselBetrag *= 0.89; //1 Euro > 0.89 GBP
			System.out.printf("%.2f Euro = %.2f GBP (Pfund)\n", a, wechselBetrag);
			
		} else if(gastland.equalsIgnoreCase("Schweiz")) {
			
			wechselBetrag *= 1.08; //1 Euro > 1.08 CHF
			System.out.printf("%.2f Euro = %.2f CHF (Schweizer Franken)\n", a, wechselBetrag);
			
		} else if(gastland.equalsIgnoreCase("Schweden")) {
			
			wechselBetrag *= 10.10; //1 Euro > 10.10 SEK
			System.out.printf("%.2f Euro = %.2f SEK (Schwedische Kronen)\n", a, wechselBetrag);
			
		}
		
		return wechselBetrag;
		
	}
	
	public static double berechneMittelwert(double x, double y) {
		
		return (x+y)/2.0;
	    
	}

}
