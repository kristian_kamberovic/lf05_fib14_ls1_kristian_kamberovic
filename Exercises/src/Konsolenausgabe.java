
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgaben als Methoden definiert
		aufgabe1();
		aufgabe2();
		aufgabe3();

	}
	
	public static void aufgabe1() {
		
		System.out.print("Hey, ich hei�e Kristian Kamberovic und bin 19 Jahre alt. ");
		System.out.print("Gerade bearbeite ich die Aufgabe 1 des Arbeitsblattes: Konsolenausgabe.\n");
		
		//Modifizierung mit Anf�hrungszeichen und Zeilenumbr�che
		System.out.println("\nVerbessert:");
		System.out.println("Hey, ich hei�e Kristian Kamberovic und bin 19 Jahre alt.");
		System.out.println("Gerade bearbeite ich die Aufgabe 1 des Arbeitsblattes: \"Konsolenausgabe\".");
		
		//.print()-Anweisung: gibt den Text �ber die Konsolenausgabe aus
		//.print()-Anweisung: gibt den Text den Text �ber die Konsolenausgabe mit Zeilenumbruch aus (in einer neuen Zeile)
		
	}
	
	public static void aufgabe2() {
		
		System.out.println("Mein Weihnachtsbaum :D\n");
		
		System.out.printf("%12s%n", "*");
		System.out.printf("%13s%n", "***");
		System.out.printf("%14s%n", "*****");
		System.out.printf("%15s%n", "*******");
		System.out.printf("%16s%n", "*********");
		System.out.printf("%17s%n", "***********");
		System.out.printf("%18s%n", "*************");
		System.out.printf("%13s%n", "***");
		System.out.printf("%13s%n", "***");
		
	}
	
	public static void aufgabe3() {
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f%n", a);
		System.out.printf("%.2f%n", b);
		System.out.printf("%.2f%n", c);
		System.out.printf("%.2f%n", d);
		System.out.printf("%.2f%n", e);
		
	}

}
