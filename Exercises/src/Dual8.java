import java.util.Scanner;

public class Dual8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		dualInDez(arrayFuellen());

	}
	
	public static int [] arrayFuellen() {
		
		Scanner sc = new Scanner(System.in);
		
		int dualzahl [] = new int[8]; //in diesem Array wird die vom Benutzer eingegebene Dualzahl gespeichert
		
		System.out.print("Bitte gebe die Ziffern der 8-stellige Dualzahl nacheinander ein (nur 1 und 0): \n");
		
		//f�llt das Array mit den eingegebenen Werten bis max. 8 Ziffern
	
		int eingabe = 0;
		
		for(int i = 0; i < dualzahl.length ; i++) {
			
			eingabe = sc.nextInt();
			
			eingabe = eingabeRichtig(eingabe, 0, 1); //Pr�ft, ob es sich um eine 0 oder 1 handelt. Ansonsten wird ein Fehler ausgegebe und die M�glichkeit geboten, die Eingabe erneut durchzuf�hren.
			
			dualzahl [i] = eingabe;
			
		}
		
		return dualzahl;
		
	}
	
	//Methodem die die Dualzahl in Dezimal umwandelt
	//----------------------------------------------
	
	public static int dualInDez(int array []) {
		
		//                  2^7  2^6  2^5  2^4  2^3  2^2  2^1  2^0
		int dualWerte [] = {128,  64,  32,  16,   8,   4,   2,   1};
		
		int dezimalzahl = 0; //Dualzahl wird in Dezimalzahl umgerechnet (z.B. 1 * 2^7 + 0 * 2^6 + 1 * 2^5 + ...) und in dieser Variable gespeichert
		
		//Umrechnung der Dualzahl in Dezimalzahl
		
		for(int i = 0; i < array.length; i++) {
			
			if(array [i] == 1) {
				
				dezimalzahl += dualWerte [i];
				
			}
			
		}
		
		System.out.print("\nDualzahl: " );
		arrayAusgabe(array);
		System.out.print(" --> in Dezimalzahl: "+ dezimalzahl);
		
		return dezimalzahl;
		
	}
	
	//Methode, um ein Array in der Konsole auszugeben
	
	public static void arrayAusgabe(int [] array) {
		
		System.out.print("");
		
		for(int i = 0; i < array.length; i++) {
			
			System.out.print(array [i] + " ");
			
		}
		
	}
	
	//Methode, die �berpr�ft, ob die Eingabe richtig ist und zeigt ggf. eine Fehlermeldung
	//----------------------------------------------------------
	
	public static int eingabeRichtig(int eingabe, double untereGrenze, double obereGrenze) {
    	
		Scanner sc = new Scanner(System.in);
		
    	boolean eingabeRichtig = false;
    
    	while(eingabeRichtig != true) {
    		
    		if(eingabe >= untereGrenze && eingabe <= obereGrenze) {
    			
   				eingabeRichtig = true;
    			
   			} else {
   				
   				error("Bitte erneut eingeben: ");
   				
   				System.out.print("\n");
   				
   				eingabe = sc.nextInt();
   			
   			}
    		
    	}
    		
    	return eingabe;
    	
    }
	
	public static void error(String text) {
		
		System.out.print("\n" + "Eingabe ung�ltig.");
	    	
	   	System.out.print("\n" + text);
	    	
	}

}
