import java.util.Scanner;

public class Ersatzwiderstand {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		
		//aufrufen der Methode '.liesDouble("String text")' der Klasse 'PCHaendler' zum einlesen des 'double'-Werts mit Anzeige eines Texts (String)
		double r1 = PCHaendler.liesDouble("Gib den Wert von r1 ein: ");
		double r2 = PCHaendler.liesDouble("Gib den Wert von r2 ein: ");
		
		double widerstandReihenschaltung = reihenschaltung(r1, r2);
		double widerstandParallelschaltung = parallelschaltung(r1, r2);
		
		System.out.printf("Ersatzwiderstand der Reihenschaltung: %.2f%n", widerstandReihenschaltung);
		System.out.printf("Ersatzwiderstand der Parallelschaltung: %.2f%n", widerstandParallelschaltung);

	}
	
	public static double reihenschaltung(double r1, double r2){
		
		double ersatzwiderstand = r1 + r2;
		
		return ersatzwiderstand;
		
	}
	
	public static double parallelschaltung(double r1, double r2) {
		
		double ersatzwiderstand = (1/r1) + (1/r2);
		
		return ersatzwiderstand;
		
	}

}
