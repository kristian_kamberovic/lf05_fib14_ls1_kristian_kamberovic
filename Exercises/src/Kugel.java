import java.util.Scanner;

public class Kugel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
	}
	
	public static double umfangKugel(double radius) {
		
		double umfang = 2 * Math.PI * radius;
		
		return umfang;
		
	}
	
	public static double oberflaecheKugel(double radius) {
		
		double oberflaeche = 4 * Math.PI * Math.pow(radius, 2);
		
		return oberflaeche;
		
	}
	
	public static double volumen(double radius) {
		
		double volumen = (4/3) * Math.PI * Math.pow(radius, 3);
		
		return volumen;
		
	}

}
