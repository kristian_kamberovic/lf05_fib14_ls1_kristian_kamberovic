
public class MyMath {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println(pythagoras(2.0,2.0));

	}
	
	public static int pow(int basis, int potenz) {
		
		int ergebnis = 1;
		
		for(int i = 0; i<potenz; i++) {
			
			ergebnis *= basis;
			
		}
		
		return ergebnis;
		
	}
	
	public static int fak(int zahl) {
		
		int ergebnis = 1;
		
		for(int i = 0; i<zahl; i++) {
			
			ergebnis *= i+1;
			
		}
		
		return ergebnis;
		
	}
	
	public static double root(double zahl, int exponent) {
		
		return Math.pow(zahl, 1.0/exponent);
		
	}
	
	public static double pythagoras(double kathete1, double kathete2) {
		
		double hypotenuse = Math.sqrt(Math.pow(kathete1, 2) + Math.pow(kathete2, 2));
		
		return hypotenuse;
		
	}

}
