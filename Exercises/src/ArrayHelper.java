import java.lang.reflect.Array;

public class ArrayHelper {

	public static void main(String[] args) {
		
		int [] zahlen = {0, 2, 3, 4, 5, 6};
		
		convertArrayToString(changeAndReturnOrderOfArray(zahlen));
		

	}
	
	public static void convertArrayToString(int [] zahlen) {
	
		for(int i = 0; i < zahlen.length; i++) {
		
			System.out.print(zahlen[i] + ", ");
			
		}
		
	}
	
	public static void changeOrderOfArray(int [] array) {
		
		for(int i = 0; i < array.length; i++) {
			
			for(int j = i; j < array.length - 1; j++) {
				
				int tmp = array [j];
				array [j] = array [array.length-1];
				array [array.length-1] = tmp;
				
			}
			
		}
	
	}
	
	public static int [] changeAndReturnOrderOfArray(int [] array) {
		
		int [] arrayToReturn = new int [array.length];
		
		for(int i = 0; i < array.length; i++) {
			
			for(int j = i; j < array.length - 1; j++) {
				
				int tmp = array [j];
				array [j] = array [array.length-1];
				array [array.length-1] = tmp;
				
			}
			
			arrayToReturn [i] = array [i];
			
			
		}
		
		return arrayToReturn;
		
	}
	
}
