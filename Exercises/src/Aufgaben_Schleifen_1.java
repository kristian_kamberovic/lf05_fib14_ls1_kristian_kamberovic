
import java.util.Scanner;

public class Aufgaben_Schleifen_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		summe();

	}
	
	public static void zaehlen() {
		
		System.out.print("Bis zu welcher Zahl n soll gez�hlt werden? ");
		
		Scanner eingabe = new Scanner(System.in); //Scanner zum Einlesen der eingegebenen Zahl
		int n = eingabe.nextInt(); //Speichern des eingegebenen Wertes in einer Variablen
		
		//Zahlen aufw�rts z�hlen
		//----------------------
		
		System.out.println("\n"+ "aufw�rts: ");
		
		for(int i = 1; i <= n; i++) { //Z�hlt Zahlen von 1 bis n
		
			System.out.print(i + " ");
			
		}
		
		//Zahlen abw�rts runterz�hlen
		//---------------------------
		
		System.out.println("\n\n"+ "abw�rts: ");
		
		for(int i = n; i > 0; i--) { //Z�hlt Zahlen von n bis 1
		
			System.out.print(i + " ");
			
		}
		
	}
	
	public static void summe() {
		
		System.out.print("Bitte geben Sie eine Zahl ein: ");
		
		Scanner eingabe = new Scanner(System.in);
		int n = eingabe.nextInt();
		
		//Summe der Zahlenfolgen von 1 bis n
		//----------------------------------
		
		int summe = 0;
		
		for(int i = 1; i <= n; i++) {
			
			summe += i;
			
		}
		
		System.out.println("\n" + "Summe von 1 bis " + n + ": " + summe);
		
		
		//Summe aller geraden Zahlen bis n
		//--------------------------------
		
		int summeGeradeZahlen = 0;
		
		for(int i = 2; i <= n; i += 2) {
			
			summeGeradeZahlen += i;
			
		}
		
		System.out.println("\n" + "Summe aller geraden Zahlen bis " + n + ": " + summeGeradeZahlen);
		
	}

}
