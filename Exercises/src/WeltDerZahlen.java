/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Kristian Kamberovic >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e ~ 150 Milliarden (zwischen 100 und 200 Milliarden Sterne)
    int anzahlSterne = 150; 
    
    // Wie viele Einwohner hat Berlin? -> in Millionen
    double bewohnerBerlin = 3.66; 
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = (19*365); 
    
    // Wie viel wiegt das schwerste Tier der Welt? -> �ber 150 Tonnen (Blauwal) 
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = (150*1000);  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat? -> 17.130.000 km� (Russland)
    int flaecheGroessteLand = 17130000;
    
    // Wie gro� ist das kleinste Land der Erde? -> 0,44 km� (Vatikanstadt)
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in unserer Milchstra�e: ~ "+anzahlSterne+" Milliarden");
    
    System.out.println("Einwohnerzahl in Berlin: ~ "+bewohnerBerlin+" Millionen");
    
    System.out.println("Mein Alter in Tage: "+alterTage+" (19 Jahre)");
    
    System.out.println("Schwerste Tier der Welt: Blauwal ("+gewichtKilogramm+" kg)");
    
    System.out.println("Gr��te Land der Welt: Russland ("+flaecheGroessteLand+" km�)");
    
    System.out.println("Kleinste Land der Welt: Vatikanstadt ("+flaecheKleinsteLand+" km�)");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
