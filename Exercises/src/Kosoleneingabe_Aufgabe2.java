import java.util.Scanner;

public class Kosoleneingabe_Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Erstellen eines Scanners
		Scanner scanner = new Scanner(System.in);

		System.out.print("Guten Tag! ");
		
		//Nachfrage nach den Namen + einlesen des Namens (String) �ber den Scanner in der Variable 'name'
		System.out.println("Wie lautet Ihr Name?");
		String name = scanner.next();
		
		//Nachfrage nach Alter (int) + einlesen des Alters �ber den Scanner in der Variable 'alter'
		System.out.println("\nHallo " + name + ", wie alt sind Sie denn?");
		int alter = scanner.nextInt();
		
		//�berpr�fen ob Angaben korrekt sind -> 'y' steht f�r yes und 'n' steht f�r no
		System.out.println("\nSind die Angaben richtig? (y/n)");
		
		//Ausgabe der eingegebenen Daten
		System.out.println("Name: " + name + ", Alter: " + alter+" Jahre alt");
		
		//Eingabe wird durch Scanner eingelesen und in Variable 'eingabe' gespeichert
		char eingabe = scanner.next().charAt(0);
		
		boolean angabenRichtig = false;
		
		//�berpr�fung und Auswertung der Eingabe
		if(eingabe == 'y') {
			
			System.out.println("\nVielen Dank!");
			
		} else {
			
			System.out.println("Hmm... Wir �berpr�fen das mal.");
			
		}
		
	}

}
