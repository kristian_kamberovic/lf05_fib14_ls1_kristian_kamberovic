import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe {

	public static void main(String[] args) { // Hier startet das Programm

		 // Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 int zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 int zahl2 = myScanner.nextInt();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnisAddition zugewiesen.
		 int ergebnisAddition = zahl1 + zahl2;
		 
		 //Subtraktion der Variablen zahl1 und zahl2
		 int ergebnisSubtraktion = zahl1 - zahl2;
		 
		 //Multiplikation der Variablen zahl1 und zahl2
		 int ergebnisMultiplikation = zahl1 * zahl2;
		 
		 //Division der Variablen zahl1 und zahl2
		 double ergebnisDivision = (double) zahl1/zahl2;

		 System.out.print("\n\n\nErgebnis der Addition lautet: ");
		 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAddition);
		 
		 System.out.print("\nErgebnis der Subtraktion: ");
		 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSubtraktion);
		 
		 System.out.print("\nErgebnis der Multiplikation: ");
		 System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMultiplikation);
		 
		 System.out.print("\nErgebnis der Division: ");
		 System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDivision);
		 
		 myScanner.close();

	 }
		
}
