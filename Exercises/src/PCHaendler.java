import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Benutzereingaben lesen
		String artikel = liesString("Was m�chten Sie bestellen?");

		int anzahl = liesInt("Geben Sie die Anzahl ein: ");

		double preis = liesDouble("Geben Sie den Nettopreis ein: ");

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
	}
	
	static Scanner myScanner = new Scanner(System.in);
	
	public static String liesString(String text) {
		
		System.out.println(text);
		
		String eingabe = myScanner.next();
		
		return eingabe;
		
	}
	
	public static int liesInt(String text) {
		
		System.out.println(text);
		
		int eingabe = myScanner.nextInt();
		
		return eingabe;
		
	}
	
	public static double liesDouble(String text) {
		
		System.out.println(text);
		
		double eingabe = myScanner.nextDouble();
		
		return eingabe;
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		
		double nettogesamtpreis = anzahl * nettopreis;
		
		return nettogesamtpreis;
		
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		return bruttogesamtpreis;
		
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		System.out.println("\t\tRechnung");
		System.out.printf("\t\tNetto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\tBrutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "% mwst");
		
	}

}
