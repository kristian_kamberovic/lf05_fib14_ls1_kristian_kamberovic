
public class Mathe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//L�nge der 1. Kathete k1 in cm
		double k1 = 5;
		
		//L�nge der 2. Kathete k2 in cm
		double k2 = 6;
		
		double hypotenuse = hypotenuse(k1, k2);
		
		System.out.println("h = Wurzel aus ((" + k1 + " cm)^2 + (" + k2 + " cm)^2) = " + hypotenuse + " cm");

	}
	
	 public static double quadrat(double a) {
		
		 double ergebnis = a*a;
		 
		 return ergebnis;
		 
	 }
	 
	 public static double hypotenuse(double kathete1, double kathete2) {
		 
		 double h = Math.sqrt(quadrat(kathete1)+quadrat(kathete2));
		 
		 return h;
		 
	 }

}
